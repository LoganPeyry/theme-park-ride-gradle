FROM openjdk:12-alpine
VOLUME /tmp
ADD /build/libs/*.jar theme-park-ride-gradle.jar
EXPOSE 5000
ENTRYPOINT exec java -jar theme-park-ride-gradle-0.0.1-SNAPSHOT.jar
HEALTHCHECK --interval=1m --timeout=30s --retries=3 CMD curl --fail http://localhost:5000 || exit 1
